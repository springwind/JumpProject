

/// <summary>
///  사용될 색상 타입을 나타내기 위한 열거 형식입니다.
/// </summary>
public enum ColorType : sbyte
{
    Red         = 0,
    Yellow      = 1,
    Sky         = 2,
    Magenta     = 3,
    White       = 4,
    Orange      = 5,
    Green       = 6,
}